package bap.metrics.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UpDownController {

  @Getter(AccessLevel.PRIVATE)
  private long counter;

  public UpDownController(MeterRegistry meterRegistry) {
    meterRegistry.gauge("bap_up_down", this, UpDownController::getCounter);
  }

  @PostMapping("up")
  public Count up() {
    counter++;
    return new Count(Direction.UP);
  }

  @PostMapping("down")
  public Count down() {
    counter--;
    return new Count(Direction.DOWN);
  }

  public enum Direction {
    @JsonProperty("up")
    UP,
    @JsonProperty("down")
    DOWN
  }

  public record Count(Direction direction) {}
}
