
# BAP metrics example service

A spring boot service that exposes its metrics to prometheus.

## Exposed metrics

- `bap_up_down` a _gauge_ that measures an artificial counter (this is an example custom metric)
- _all spring boot default metrics_

Metrics are exposed via the _actuator endpoint_:

```sh
curl http://localhost:8080/actuator/prometheus
```

## Endpoints

### `/up`

Increases our custom metric by 1

```sh
curl -X POST http://localhost:8080/up
# {"direction":"up"}
```

### `/down`

Decreases our custom metric by 1

```sh
curl -X POST http://localhost:8080/down
# {"direction":"down"}
```